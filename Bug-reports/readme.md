# Bug reports
### About the documents
This document presents the cumulative bug reports and links them to the executed test cases [link to TC execution here.](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Tests-execution/Test-execution-report.pdf)

## Table of Contents
| Bug reports (pdf) | Issues screenshots (png) |
|-------------|--------------------|
| [MRBUG-553](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/ef3fe59da01e63a252f72d6b8d0f9a796cd93d67/Bug-reports/docs/MRBUG-553.pdf)   | [MRBUG-553](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Bug-reports/docs/MRBUG-553.png)          |
| [MRBUG-554](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Bug-reports/docs/MRBUG-554.pdf)          | [MRBUG-554](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Bug-reports/docs/MRBUG-554.png)
| [MRBUG-555](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Bug-reports/docs/MRBUG-555.pdf)   | [MRBUG-555](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Bug-reports/docs/MRBUG-555.png)          |
| [MRBUG-556](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Bug-reports/docs/MRBUG-556.pdf)   | [MRBUG-556](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Bug-reports/docs/MRBUG-556.png)          |
| [MRBUG-557](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Bug-reports/docs/MRBUG-557.pdf)   | [MRBUG-557](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Bug-reports/docs/MRBUG-557.png)          |
| [MRBUG-558](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Bug-reports/docs/MRBUG-558.pdf)   | [MRBUG-558](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Bug-reports/docs/MRBUG-558.png)          |
| [MRBUG-559](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Bug-reports/docs/MRBUG-559.pdf)   | [MRBUG-559](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Bug-reports/docs/MRBUG-559.png)          |
| [MRBUG-560](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Bug-reports/docs/MRBUG-560.pdf)   | [MRBUG-560](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Bug-reports/docs/MRBUG-560.png)          |

