# Developer Tools

### About the document
This is the example of DevTools use to inspect the webpage

#### DevTools elements
[DevTools elements!](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Other-tools/pictures/DevTools/DevTools-elements.png)

#### DevTools console
[DevTools console!](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Other-tools/pictures/DevTools/DevTools-console.png)

#### DevTools network
[DevTools network!](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Other-tools/pictures/DevTools/DevTools-network.png)

#### DevTools sources
[DevTools sources!](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Other-tools/pictures/DevTools/DevTools-sources.png)

#### DevTools lighthouse
[DevTools lighthouse!](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Other-tools/pictures/DevTools/DevTools-lighthouse.png)
