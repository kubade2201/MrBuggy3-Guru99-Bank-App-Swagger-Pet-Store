# Representative testing project
### MrBuggy3 | Guru99 Bank App | Swagger Pet Store

### About the project

* The purpose of the project was to assess the user registration functionality of the MrBuggy3 application and identify any non-compliance with the application's requirements.
* [**MrBuggy3**](http://mrbuggy.pl/mrbuggy3/) is a desktop application that was used during the first round of the Polish Software Testing Championship to qualify the participants in the final round. 
* The **MrBuggy3** app is made available to all those who are willing to test it. This gives every testing enthusiast the opportunity to develop and test their testing skills. 
* The target group of the application’s users consists of professional testers, software testing enthusiasts in Poland and all people who want to face testing tasks .
#### The project also contain
- Test automation of the User Interface in [Guru 99 Bank App](https://demo.guru99.com/V4/), using Selenium WebDriver, along with examples of BDD-written tests.
* REST-API requests sent to the [Swagger Pet Store](https://petstore.swagger.io/) using Postman.
#### Test environment 
* Microsoft Windows 11 Version	10.0.22621 Build 22621
* Web browsers:
  * Microsoft Edge Version 112.0.1722.64 (Official build) (64-bit)
  * Chrome 113.0.5672.63 (Official Build) (64-bit)
#### Test objects
* MrBuggy3
* Guru99 Bank App 
* Swagger Pet Store
#### Main area of testing
* User registration (**MrBuggy3**)
* User creation from the admininstrator account (**Guru 99 Bank App**)
* REST API request responses (**Swagger Pet Store**)

# Table of Content
 ### [1. Specification analysis](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/tree/main/Spec-analysis)
* [The MrBuggy3 specification](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/tree/main/Spec-analysis)
* [The specification review](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Spec-analysis/2.%20Requirements_review.pdf) 
* [The App specification after revision](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Spec-analysis/3.%20Mr_Buggy_3_specification-after-review.pdf)
* [Functionalities list](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Spec-analysis/4.%20Functionalities-list.md)
 ### 2. [Risks (project, product)](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/tree/main/Risks)
 ### 3. [Testing approach analysis](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/tree/main/Testing-approach)
 ### 4. [Test cases](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/tree/main/test-cases)
* [Maximum coverage test cases (written in text format)](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/test-cases/Mr%20Buggy-registration-feature.pdf)
* [Exemplary test cases from Test Management tool (Jira)](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/test-cases/Test-cases-set.pdf)
 ### 5. [Tests execution](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/tree/main/Tests-execution)
* [Tests execution report](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Tests-execution/Test-execution-report.pdf)
* [Exploratory testing approach](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Tests-execution/MRBUGGY3%20-%20exploratory%20testing%20report%20PT.pdf)
 ### 6. [Bug reports](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/tree/main/Bug-reports)
 ### 7. [Additional tools (not associated with MrBuggy3 app)](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/tree/main/Other-tools)
 #### Guru-99 Bank App
---
* [Selenium WebDriver automation example (Guru-99 Bank App)](https://gitlab.com/kubade2201/Guru99-Bank-automated-testing)
* [Behaviour Driver Development  (Guru-99 Bank App test case)](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Other-tools/BDD-TC.md)
* [Chrome DevTools (Guru-99 Bank App)](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Other-tools/DevTools.md)
---
#### Swagger Pet Store
* [REST API - Postman (GET, POST, PUT, DELETE)](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Other-tools/REST-API-Postman.md)

<!--
# :construction_worker: :construction: `Under development`:construction:
You know that feeling when you keep digging and the hole just keeps getting deeper? That's what I did with this section.
> ** Don't worry, there will be more to come soon!** :do_not_litter:



