# MrBuggy3 user registration - tests execution
### This section refers to tests execution of registration functionality of MrBuggy3 App. 
### Tests execution step have been divided into two documents:
1. [Tests execution report](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Tests-execution/Test-execution-report.pdf)
2. [Exploratory testing session](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Tests-execution/MRBUGGY3%20-%20exploratory%20testing%20report%20PT.pdf)
