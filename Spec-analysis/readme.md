# Task description

> **The directory contain**
> * [The MrBuggy3 specification](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Spec-analysis/1.%20Mr_Buggy_3_specification.pdf)
> * [The specification review](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Spec-analysis/2.%20Requirements_review.pdf) 
> * [The App specification after revision](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Spec-analysis/3.%20Mr_Buggy_3_specification-after-review.pdf)
> * [Functionalities list](https://gitlab.com/kubade2201/MrBuggy3-Guru99-Bank-App-Swagger-Pet-Store/-/blob/main/Spec-analysis/4.%20Functionalities-list.md)
